# rsink

My personal script for backing up my data using [rsync](https://linux.die.net/man/1/rsync).

## Usage

You can change the configuration in the [`rsink.conf`](./rsink.conf), [`include_files.conf`](./include_files.conf) and [`exclude_files.conf`](./exclude_files.conf) files.

### `rsink.sh`

This script allows to perform a backup of wanted files on an external hard drive.

```
# Change the permissions to execute the script
chmod +x rsink.sh

# Execute the script
bash rsink.sh
```

## License

The project is licensed under the MIT License - see the [`LICENSE`](./LICENSE) file for details.
