#!/usr/bin/env bash

## Error handling
set -o errexit
set -o pipefail
set -o noclobber
set -o nounset
set -o functrace

## Switch to the directory where the script is executed
cd "$(dirname "$(realpath "$0")")";

## Load configuration
source rsink.conf

## Script
echo "The script is run in multiple phases:"
echo
echo "  1. The backup destination and log folders are created."
echo "  2. The backup is run dry. A log file is created. No changes will be made."
echo "  3. Review the log file to see the changes that would be done."
echo "  4. Decide to continue the backup process or quit the script."
echo
read -r -p "Press any key to continue the backup process or <Ctrl + C> to exit."

rsyncArguments=(
  # recurse into directories
  --recursive
  # disable incremental recursion so all files are considered from the start
  # --no-inc-recursive
  # copy symlinks as symlinks
  --links
  # preserve permissions
  --perms
  # affect directory and/or file permissions
  --chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r
  # preserve modification times
  --times
  # preserve owner
  --owner
  # preserve group
  --group
  # preserve device files
  --devices
  # preserve special files
  --specials
  # preserve executability
  --executability
  # find deletions during, delete after
  --delete-delay
  # also delete excluded files from dest dirs
  --delete-excluded
  # read list of source-file names from FILE
  --files-from="${BACKUP_INCLUDE_FILE}"
  # read exclude patterns from FILE
  --exclude-from="${BACKUP_EXCLUDE_FILE}"
  # keep partially transferred files
  --partial
  # compress file data during the transfer
  --compress
  # output numbers in a human-readable format
  --human-readable
  # show progress during transfer
  --progress
  # change the output to display total progress
  --info=progress2
  # give some file-transfer stats
  --stats
)

echo "Creating the log folder ($(dirname "${BACKUP_LOG_FILE}"))..."

mkdir -p "$(dirname "${BACKUP_LOG_FILE}")"

echo "Dry backing up..."

rsync "${rsyncArguments[@]}" --dry-run "${BACKUP_SOURCE}" "${BACKUP_DESTINATION}" 2>&1 | tee "${BACKUP_LOG_FILE}"

echo "Dry backup done. The log file can be reviewed in '${BACKUP_LOG_FILE}'."
echo
read -r -p "Press any key to continue the backup process or <Ctrl + C> to exit."

echo "Creating the backup folder (${BACKUP_DESTINATION})..."

mkdir -p "${BACKUP_DESTINATION}"

rsync "${rsyncArguments[@]}" "${BACKUP_SOURCE}" "${BACKUP_DESTINATION}" 2>&1 | tee "${BACKUP_LOG_FILE}"

sync

echo "The backup is done!"
